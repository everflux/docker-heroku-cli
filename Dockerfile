#simple heroku-cli docker installation
#build with: docker build -t trion/heroku-cli .
#Warning: heroku does auto-update, you should rebuild this image before usage...
FROM debian:jessie

MAINTAINER trion development GmbH "info@trion.de"

ARG USER_ID=1000
ARG USER_HOME_DIR="/user"

ENV HOME $USER_HOME_DIR
WORKDIR /$USER_HOME_DIR

ENV NPM_CONFIG_LOGLEVEL warn


RUN  mkdir -p $USER_HOME_DIR \
    && apt-get update && apt-get install -y --no-install-recommends git wget ca-certificates \
    && wget https://cli-assets.heroku.com/branches/stable/heroku-linux-amd64.tar.gz \
    && echo "Downloaded archive SHA: $( sha256sum heroku-linux-amd64.tar.gz | cut -d' ' -f1 )" \
    && tar -xzf heroku-linux-amd64.tar.gz -C /usr/local/lib/ \
    && ln -s /usr/local/lib/heroku/bin/heroku /usr/local/bin/heroku \
    && rm heroku-linux-amd64.tar.gz \
    && apt-get purge -y wget \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && chown -R $USER_ID $USER_HOME_DIR


ENTRYPOINT ["/usr/local/bin/heroku"]

USER $USER_ID
