# docker-heroku-cli

Docker image for heroku CLI

*Note:* heroku has an auto-update, does not publish versioned artifacts.
To get a stable image it would have to include a patched build.

This image uses the latest-stable heroku cli standalone download, so you *should build it yourself*.

Example usage
```
docker run --rm trion/heroku-cli version
```

```
docker run --rm -e HEROKU_API_KEY="abc-123123123" trion/heroku-cli list

```

```
docker run --rm -v ~/.netrc:/user/.netrc:ro trion/heroku-cli list
```
